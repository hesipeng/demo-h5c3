import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count : 1
  },
  mutations: {
    setCount(state,payload){
      state.count+=payload
    }
  },
  actions: {
    reqCount(store,paload){
      store.commit('setCount',paload)
    }
  },
  getters: {
    count: state => state.count
  },
  modules: {
  }
})
