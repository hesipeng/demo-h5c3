import Vue from 'vue'
import App from './App.vue'
import HelloGlobal from '@/components/HelloGlobal'
Vue.config.productionTip = false

Vue.component('HelloGlobal', HelloGlobal)
new Vue({
  render: h => h(App)
}).$mount('#app')
