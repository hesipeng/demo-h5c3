module.exports = {
    plugins: {
        // 'autoprefixer': {
        //     browsers: ['Android >= 4.0', 'iOS >= 8']
        // },

        // 'postcss-pxtorem': {
        //     rootValue({ file }) {
        //         return file.indexOf('vant') !== -1 ? 37.5 : 75;
        //     },
        // //     rootValue: 75,
        //     propList: ['*']
        // }
        'postcss-px-to-viewport': {
            viewportWidth: 375,
        }
    }
}