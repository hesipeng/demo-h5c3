import Axios from "axios";
import Vue from "vue";

var request = Axios.create({
    baseURL:'http://ttapi.research.itcast.cn'
});

Vue.prototype.$request = request;

export  default request;