import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/button',
    name: 'button',
    component: () => import('@/views/button')
  },
  {
    path: '/cell',
    name: 'cell',
    component: () => import('@/views/cell')
  }
  // {
  //   path: '/',
  //   name: '/',
  //   redirect :'/home'
  //
  // },
  // {
  //   path: '/home',
  //   name: 'home',
  //   component: () => import('@/views/home')
  // },


  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView
  // },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
